package com.sonatel.repository;

import com.sonatel.domain.Utilisateur;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Utilisateur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UtilisateurRepository  {
    public  Utilisateur save(Utilisateur utilisateur);

    public Utilisateur update(Utilisateur utilisateur);

    public  void delete(String id);
    public List<Utilisateur> findAll();

    public List<Utilisateur> findOne(String id);

    public Utilisateur findByLogin(String login);
    public List<Utilisateur> findByLoginAndPassword(String login,String password);


}
