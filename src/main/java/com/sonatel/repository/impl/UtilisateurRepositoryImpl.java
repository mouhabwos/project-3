package com.sonatel.repository.impl;

import com.sonatel.domain.Utilisateur;
import com.sonatel.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import rx.functions.ActionN;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class UtilisateurRepositoryImpl implements UtilisateurRepository {
    @Autowired
    private EntityManager em;



    public  Utilisateur save(Utilisateur utilisateur){

        em.persist(utilisateur);
        em.flush();
        return utilisateur;

    }

    public Utilisateur update(Utilisateur utilisateur){


        if (em.contains(utilisateur)) {
            em.merge(utilisateur);
        } else {
            em.persist(utilisateur);
        }
        em.flush();
        return utilisateur;

    }

    public  void delete(String id){

        em.createQuery("Delete from Utilisateur  a where a.id = " + id).executeUpdate();



    }

    public  List<Utilisateur> findAll(){

        return em.createQuery("Select a from Utilisateur a",Utilisateur.class).getResultList();

    }

    public List<Utilisateur> findOne(String id){

        return em.createQuery("Select a from Utilisateur a where a.id = " + id,Utilisateur.class).getResultList();


    }

    public Utilisateur findByLogin(String login){


        Utilisateur result = null;
        try {
            result = (Utilisateur) em.createNativeQuery("Select * from utilisateur a where a.login ='" + login + "'",Utilisateur.class).getSingleResult();
        } catch (NoResultException e) {
            System.out.println("=========================No result forund for... ");
        }
        return  result;

    }
    public List<Utilisateur> findByLoginAndPassword(String login,String password){

        System.out.println("Select * from utilisateur a where a.login ='" + login + "' AND a.jhi_password = '" + password+ "'");


        List<Utilisateur>  result = null;
        try {
            result = em.createNativeQuery("Select * from utilisateur a where a.login ='" + login + "' AND a.jhi_password = '" + password+ "'" ,Utilisateur.class).getResultList();
        } catch (NoResultException e) {
            System.out.println("=========================No result forund for... ");
        }
        return  result;

    }


}
