package com.sonatel.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    USER, ADMIN
}
