package com.sonatel.service.impl;

import com.sonatel.service.UtilisateurService;
import com.sonatel.domain.Utilisateur;
import com.sonatel.repository.UtilisateurRepository;
import com.sonatel.repository.search.UtilisateurSearchRepository;
import com.sonatel.service.dto.UtilisateurDTO;
import com.sonatel.service.mapper.UtilisateurMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Utilisateur.
 */
@Service
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {

    private final Logger log = LoggerFactory.getLogger(UtilisateurServiceImpl.class);

    private final UtilisateurRepository utilisateurRepository;

    private final UtilisateurMapper utilisateurMapper;

    private final UtilisateurSearchRepository utilisateurSearchRepository;

    public UtilisateurServiceImpl(UtilisateurRepository utilisateurRepository, UtilisateurMapper utilisateurMapper, UtilisateurSearchRepository utilisateurSearchRepository) {
        this.utilisateurRepository = utilisateurRepository;
        this.utilisateurMapper = utilisateurMapper;
        this.utilisateurSearchRepository = utilisateurSearchRepository;
    }

    /**
     * Save a utilisateur.
     *
     * @param utilisateurDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UtilisateurDTO save(UtilisateurDTO utilisateurDTO) throws Exception {

        Utilisateur UtilisateurExisting = findByLogin(utilisateurDTO.getLogin());

        // log.info("Null pointer exception : {}", UtilisateurExisting.getFirstName());



        if (UtilisateurExisting!=null )
            throw new Exception("Le login existe");

        Utilisateur utilisateur = utilisateurMapper.toEntity(utilisateurDTO);
        utilisateur = utilisateurRepository.save(utilisateur);
        UtilisateurDTO result = utilisateurMapper.toDto(utilisateur);
        utilisateurSearchRepository.save(utilisateur);
        return result;
    }
    /**
     * Save a utilisateur.
     *
     * @param utilisateurDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UtilisateurDTO update(UtilisateurDTO utilisateurDTO) throws Exception {

        Utilisateur UtilisateurExisting = findByLogin(utilisateurDTO.getLogin());

        // log.info("Null pointer exception : {}", UtilisateurExisting.getFirstName());



        if ((UtilisateurExisting != null &&  UtilisateurExisting.getId() != utilisateurDTO.getId()))
            throw new Exception("Le login existe");

        Utilisateur utilisateur = utilisateurMapper.toEntity(utilisateurDTO);
        utilisateur = utilisateurRepository.update(utilisateur);
        UtilisateurDTO result = utilisateurMapper.toDto(utilisateur);
        utilisateurSearchRepository.save(utilisateur);
        return result;
    }
    /**
     * Get all the utilisateurs.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<UtilisateurDTO> findAll() {
        log.debug("Request to get all Utilisateurs");
        return utilisateurMapper.toDto(utilisateurRepository.findAll()) ;
    }


    /**
     * Get one utilisateur by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public List<UtilisateurDTO> findOne(String id) {
        log.debug("Request to get Utilisateur : {}", id);
        return utilisateurMapper.toDto(utilisateurRepository.findOne(id));
    }

    /**
     * Delete the utilisateur by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Utilisateur : {}", id);
        utilisateurRepository.delete(id);
    }



    /**
     * Get the "email" custumer.
     *
     * @param login the login of the entity
     * @param password the password of the entity
     * @return the entity
     */
    @Override
    public List<Utilisateur>  findByLoginAndPassword(String login, String password) {
        return utilisateurRepository.findByLoginAndPassword(login,password);
    }


    /**
     * Get the "login" utilistaeur.
     *
     * @param login the login of the entity
     * @return the entity
     */
    @Override
    public Utilisateur findByLogin(String login) {
        return utilisateurRepository.findByLogin(login);
    }
}
