package com.sonatel.service;

import com.sonatel.domain.Utilisateur;
 import com.sonatel.service.dto.UtilisateurDTO;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Utilisateur.
 */
public interface UtilisateurService {

    /**
     * Save a utilisateur.
     *
     * @param utilisateurDTO the entity to save
     * @return the persisted entity
     */
    UtilisateurDTO save(UtilisateurDTO utilisateurDTO) throws Exception;
    public UtilisateurDTO update(UtilisateurDTO utilisateurDTO) throws Exception ;


        /**
         * Get all the utilisateurs.
         *
         * @return the list of entities
         */
    List<UtilisateurDTO> findAll();


    /**
     * Get the "id" utilisateur.
     *
     * @param id the id of the entity
     * @return the entity
     */
    List<UtilisateurDTO> findOne(String id);

    /**
     * Delete the "id" utilisateur.
     *
     * @param id the id of the entity
     */
    void delete(String id);



    /**
     * Get the "email" custumer.
     *
     * @param login the login of the entity
     * @param password the password of the entity
     * @return the entity
     */
    List<Utilisateur> findByLoginAndPassword(String login, String password );

    public Utilisateur findByLogin(String login);

}
