package com.sonatel.service.dto;

import com.sonatel.domain.enumeration.Role;

import java.io.Serializable;
import java.util.Objects;


public class Credential implements Serializable {



    private String login;

    private String password;



    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    @Override
    public String toString() {
        return "UtilisateurDTO{" +
             ", login='" + getLogin() + "'" +
            ", password='" + getPassword() + "'" +
             "}";
    }
}
