package com.sonatel.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sonatel.domain.Utilisateur;
import com.sonatel.service.UtilisateurService;
import com.sonatel.service.dto.Credential;
import com.sonatel.web.rest.util.HeaderUtil;
 import com.sonatel.service.dto.UtilisateurDTO;
 import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;

/**
 * REST controller for managing Utilisateur.
 */
@RestController
@RequestMapping("/api")
public class UtilisateurResource {

    private final Logger log = LoggerFactory.getLogger(UtilisateurResource.class);

    private static final String ENTITY_NAME = "project3Utilisateur";

    private final UtilisateurService utilisateurService;

    public UtilisateurResource(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    /**
     * POST  /utilisateurs : Create a new utilisateur.
     *
     * @param utilisateurDTO the utilisateurDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new utilisateurDTO, or with status 400 (Bad Request) if the utilisateur has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/utilisateurs")
    @Timed
    public ResponseEntity<UtilisateurDTO> createUtilisateur(@RequestBody UtilisateurDTO utilisateurDTO) throws URISyntaxException {
        log.debug("REST request to save Utilisateur : {}", utilisateurDTO);
        if (utilisateurDTO.getId() != null) {
            throw new RuntimeException("L'utilisateur ne peut disposer d'un ID");
        }
        UtilisateurDTO result = null;
        try {
            result = utilisateurService.save(utilisateurDTO);
        } catch (Exception e) {
            log.info("Error: ",e.getMessage());
        }
        return ResponseEntity.created(new URI("/api/utilisateurs/" + result.getId()))
            //.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.toString()))
            .body(result);
    }



    /**
     * GET  /utilisateurs : get all the utilisateurs.
     *
      * @return the ResponseEntity with status 200 (OK) and the list of utilisateurs in body
     */
    @GetMapping("/utilisateurs")
    @Timed
    public ResponseEntity<List<UtilisateurDTO>> getAllUtilisateurs() {
        log.debug("REST request to get a page of Utilisateurs");
        List<UtilisateurDTO> utilisateurDTOS  = utilisateurService.findAll();
         return new ResponseEntity<>(utilisateurDTOS, HttpStatus.OK);
    }

    /** http://192.168.1.4:8083/project3/api/utilisateurs/1 or 1=1
     * GET  /utilisateurs/:id : get the "id" utilisateur.
     *
     * @param id the id of the utilisateurDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the utilisateurDTO, or with status 404 (Not Found)
     */
    @GetMapping("/utilisateurs/{id}")
    @Timed
    public ResponseEntity<List<UtilisateurDTO>> getUtilisateur(@PathVariable String id) {
        log.debug("REST request to get Utilisateur : {}", id);
        List<UtilisateurDTO> utilisateurDTO = utilisateurService.findOne(id);
        return new ResponseEntity<>(utilisateurDTO, HttpStatus.OK);
    }

    /** http://192.168.1.4:8083/project3/api/utilisateurs/1 or 1=1
     * DELETE  /utilisateurs/:id : delete the "id" utilisateur.
     *
     * @param id the id of the utilisateurDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/utilisateurs/{id}")
    @Timed
    public ResponseEntity<Void> deleteUtilisateur(@PathVariable String id) {
        log.debug("REST request to delete Utilisateur : {}", id);
        utilisateurService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @PostMapping("/login")
    @Timed
    public ResponseEntity<List<Utilisateur>> login(@RequestBody Credential credential) throws URISyntaxException {

        List<Utilisateur> result =  utilisateurService.findByLoginAndPassword(credential.getLogin(),credential.getPassword());

        if(result.isEmpty()) {
            new Exception("Echec de l'authentification");
        }
       return new ResponseEntity<>(result, HttpStatus.OK);

    }



}
